/*
DE8,
MOTOR2: 4, 10, 4,11 4 istiqamet pini
MOTOR1: 7, 9, 7, 6 7 istiqamet
*/
#include <Arduino.h>
#include <ModbusRtu.h>
#include <window.h>
#include <EEPROM.h>

#define MODBUS_SERIAL Serial
const uint8_t modbus_rs_de_pin = 8;
const uint8_t  number_of_shift_chips = 2;
const uint8_t data_width = number_of_shift_chips * 8;
const uint8_t pulse_width_usec = 5;
const uint8_t  poll_delay_msec = 1;

int p_load_pin = 13;  // Connects to Parallel load pin the 165
int clock_enable_pin = 3;  // Connects to Clock Enable pin the 165
int data_pin = 5; // Connects to the Q7 pin the 165
int clock_pin = 12; // Connects to the Clock pin the 165

/*****VARIABLE DEFINITIONS*****/

const uint8_t number_of_windows = 2;
const uint32_t modbus_baud = 19200;
uint8_t assigned_id;
uint16_t wind_speed;
uint16_t modbus_data[5] = { 0,0,0,1,1234 };
Modbus mb_slave(assigned_id, MODBUS_SERIAL, modbus_rs_de_pin);

enum slave_def {
	mb_w1_cmd,
	mb_w2_cmd,
	mb_lim_over,
	mb_e_flag,
	eeprom_verify
};
enum motor_movement_def
{
	motor_stop,
	motor_cw,
	motor_ccw
};
enum motor_action_def
{
	MOTOR_IDLE,
	MOTOR_REVERSEACTION,
	MOTOR_ACTION
};
/******************************/

/*****FUNCTION DEFINITIONS*****/

/******************************/
window windows[2] = { window(7, 9, 6), window(4, 10, 11) };

void write_int_array_into_eeprom(int address, uint16_t* numbers, int array_size = 0)
{
	int idx = address;
	int arrsz;
	if (array_size == 0)arrsz = sizeof(numbers) / sizeof(numbers[0]); else arrsz = array_size;
	for (int i = 0; i < arrsz; i++)
	{
		EEPROM.update(idx, numbers[i] >> 8);
		EEPROM.update(idx + 1, numbers[i] & 0xFF);
		idx += 2;
	}
}

void get_int_array_from_eeprom(int address, uint16_t* numbers, int array_size = 0) {
	int idx = address;
	int arrsz;
	if (array_size == 0)arrsz = sizeof(numbers) / sizeof(numbers[0]); else arrsz = array_size;
	for (int i = 0; i < arrsz; i++) {
		uint8_t hw, lw;
		EEPROM.get(idx, hw);
		EEPROM.get(idx + 1, lw);
		idx += 2;
		numbers[i] = hw << 8 | lw;
	}

}
void saveToEEPROM() {
	write_int_array_into_eeprom(0, modbus_data);
}

unsigned int getShiftRegisterValue()
{
	long bit_val;
	unsigned int bytes_val = 0;

	digitalWrite(clock_enable_pin, HIGH);
	digitalWrite(p_load_pin, LOW);
	delayMicroseconds(pulse_width_usec);
	digitalWrite(p_load_pin, HIGH);
	digitalWrite(clock_enable_pin, LOW);

	for (int i = 0; i < data_width; i++)
	{
		bit_val = digitalRead(data_pin);
		bytes_val |= (bit_val << ((data_width - 1) - i));
		digitalWrite(clock_pin, HIGH);
		delayMicroseconds(pulse_width_usec);
		digitalWrite(clock_pin, LOW);
	}

	return(bytes_val) >> 8;
}
unsigned int getShiftRegisterValue1()
{
	long bit_val;
	unsigned int bytes_val = 0;

	digitalWrite(clock_enable_pin, HIGH);
	digitalWrite(p_load_pin, LOW);
	//delayMicroseconds(pulse_width_usec);
	digitalWrite(p_load_pin, HIGH);
	digitalWrite(clock_enable_pin, LOW);

	for (int i = 0; i < data_width; i++)
	{
		bit_val = digitalRead(data_pin);
		bytes_val |= (bit_val << ((data_width - 1) - i));
		digitalWrite(clock_pin, HIGH);
		//delayMicroseconds(pulse_width_usec);
		digitalWrite(clock_pin, LOW);
	}

	return (bytes_val) & 0xff;
}



void setup()
{
	pinMode(p_load_pin, OUTPUT);
	pinMode(clock_enable_pin, OUTPUT);
	pinMode(clock_pin, OUTPUT);
	pinMode(data_pin, INPUT);

	digitalWrite(clock_pin, LOW);
	digitalWrite(p_load_pin, HIGH);

	assigned_id = getShiftRegisterValue() + 2;

	MODBUS_SERIAL.begin(modbus_baud);
	MODBUS_SERIAL.println(assigned_id);
	mb_slave.setID(assigned_id);
	mb_slave.start();
	get_int_array_from_eeprom(0, modbus_data);
	if (modbus_data[eeprom_verify] != 1234) {
		Serial.println("FAIL");
		for (uint8_t i = 0; i < 4; i++)
			modbus_data[i] = 0;
		modbus_data[eeprom_verify] == 1234;
	}
	else
		Serial.println("DONE");

	
}
uint8_t last = 0;
int pos = 0;
void loop()
{
	mb_slave.poll(modbus_data, 5);
	uint8_t ss = getShiftRegisterValue1();

	if (modbus_data[mb_lim_over] == 1) {
		windows[0].setState(modbus_data[mb_w1_cmd]);
		windows[1].setState(modbus_data[mb_w2_cmd]);
	}
	else
	{
		windows[0].setState(motor_stop);
		windows[1].setState(motor_stop);
	}
	windows[0].update();
	windows[1].update();
	if (modbus_data[mb_e_flag] == 1) {
		saveToEEPROM();
		modbus_data[mb_e_flag] = 0;
	}
}
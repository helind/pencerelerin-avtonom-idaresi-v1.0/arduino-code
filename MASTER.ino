/*
  DE8,
  MOTOR2: 4, 10, 4,11 4 istiqamet pini
  MOTOR1: 7, 9, 7, 6 7 istiqamet
*/
#include <Arduino.h>
#include <ModbusRtu.h>
#include <ModbusMaster.h>

/******COMMUNICATION PORTS*******/
#define DEBUG_SERIAL Serial		//
#define MB_SLAVE_SERIAL Serial1	//
#define MB_MASTER_SERIAL Serial2//
/********************************/


/*********CONSTANT VARIABLE DEFINITIONS******/
const uint32_t debug_baud = 115200U;		//
const uint32_t mb_slave_baud = 19200U;		//
const uint32_t mb_master_baud = 19200U;		//
const uint8_t mb_slave_de = 8U;				//
const uint8_t mb_master_de = 9U;			//
const uint8_t mb_data_size = 10U;				//
const uint32_t dbg_buf_len = 256U;			//
/********************************************/

/**************************NON_CONSTANT VARIABLE DEFINITIONS*****************/
unsigned long pperiod = 0;													//
bool fflag = false;															//
bool anti_done_once = false;												//
bool done_once = false;														//
bool man_flag = false;														//
char dbg_buffer[dbg_buf_len];												//
uint16_t modbus_data[mb_data_size] = { 0 };									//
uint8_t slave_cnt = 0;														//
uint8_t id_list[32] = { 0 };												//
Modbus mb_slave(88, MB_SLAVE_SERIAL, mb_slave_de);  						//
ModbusMaster mb_master;														//
/****************************************************************************/

/**************MACRO DEFINITIONS************************/
#define PRINTFN(FORMAT, ...){\
    memset(dbg_buffer, 0, dbg_buf_len);         \
    sprintf(dbg_buffer, FORMAT, ##__VA_ARGS__); \
    strcat(dbg_buffer, "\r\n");                 \
    DEBUG_SERIAL.print(dbg_buffer); }

#define DBG(str) DEBUG_SERIAL.print(F(str))
#define DBGN(str) DEBUG_SERIAL.println(F(str))
/*******************************************************/
enum slave_def
{
	mb_w1_cmd,
	mb_w2_cmd,
	mb_lim_over,
	mb_eflag
};

enum mb_def
{
	mb_windspeed,
	mb_windspeedlim,
	mb_sel_id,
	mb_sel_win,
	mb_win_cmd,
	mb_win_save_flag,
	mb_eeprom_save_flag,
	mb_lim_over_flag,
	mb_manualcont,
	mb_windspeedmin
};

void mb_master_pre_transmission()
{
	digitalWrite(mb_master_de, HIGH);
}

void mb_master_post_transmission()
{
	digitalWrite(mb_master_de, LOW);
}



uint16_t read_holding(uint8_t id, uint8_t addr)
{
	fflag = false;
	mb_master.begin(id, MB_MASTER_SERIAL);
	uint16_t result = mb_master.readHoldingRegisters(addr, 1);
	PRINTFN("READING ID: %d, ADDRESS: %d", id, addr);
	switch (result)
	{
	case mb_master.ku8MBSuccess:
		DBGN("COMMAND SUCCESFUL");
		result = mb_master.getResponseBuffer(0);
		fflag = true;
		break;
	case mb_master.ku8MBSlaveDeviceFailure:
		DBGN(
			"An unrecoverable error occurred while the server (or slave) was attempting to perform the requested action.");
		break;
	case mb_master.ku8MBResponseTimedOut:
		DBGN("The entire response was not received within the timeout period.");
		break;
	case mb_master.ku8MBInvalidSlaveID:
		DBGN("The slave ID in the response does not match that of the request.");
		break;
	case mb_master.ku8MBInvalidFunction:
		DBGN("The function code in the response does not match that of the request.");
		break;
	case mb_master.ku8MBInvalidCRC:
		DBGN("The CRC in the response does not match the one calculated.");
		break;
	case mb_master.ku8MBIllegalFunction:
		DBGN("The function code received in the query is not an allowable action for the server (or slave).");
		break;
	case mb_master.ku8MBIllegalDataAddress:
		DBGN("The data address received in the query is not an allowable address for the server (or slave).");
		break;
	case mb_master.ku8MBIllegalDataValue:
		DBGN(" A value contained in the query data field is not an allowable value for server (or slave).");
		break;
	default: break;
	}
	return 0;
}

uint8_t write_holding_reg(uint8_t id, uint8_t addr, uint16_t val, bool mult = false, uint8_t qnt = 1)
{
	mb_master.begin(id, MB_MASTER_SERIAL);
	uint8_t result;
	if (!mult) result = mb_master.writeSingleRegister(addr, val); else
	{
		mb_master.setTransmitBuffer(0, val);
		mb_master.setTransmitBuffer(1, val);
		result = mb_master.writeMultipleRegisters(mb_w1_cmd, 2);
	}
	PRINTFN("WRITING ID: %d, ADDRESS: %d, VALUE: %d", id, addr, val);
	switch (result)
	{
	case mb_master.ku8MBSuccess:
		DBGN("COMMAND SUCCESFUL");
		break;
	case mb_master.ku8MBSlaveDeviceFailure:
		DBGN(
			"An unrecoverable error occurred while the server (or slave) was attempting to perform the requested action.");
		break;
	case mb_master.ku8MBResponseTimedOut:
		DBGN("The entire response was not received within the timeout period.");
		break;
	case mb_master.ku8MBInvalidSlaveID:
		DBGN("The slave ID in the response does not match that of the request.");
		break;
	case mb_master.ku8MBInvalidFunction:
		DBGN("The function code in the response does not match that of the request.");
		break;
	case mb_master.ku8MBInvalidCRC:
		DBGN("The CRC in the response does not match the one calculated.");
		break;
	case mb_master.ku8MBIllegalFunction:
		DBGN("The function code received in the query is not an allowable action for the server (or slave).");
		break;
	case mb_master.ku8MBIllegalDataAddress:
		DBGN("The data address received in the query is not an allowable address for the server (or slave).");
		break;
	case mb_master.ku8MBIllegalDataValue:
		DBGN(" A value contained in the query data field is not an allowable value for server (or slave).");
		break;
	default: break;
	}
	return result;
}

uint8_t get_slave_count()
{
	DBGN("GETTING SLAVE COUNT");

	uint8_t cnt = 0;
	uint8_t indx = 0;
	for (uint8_t i = 2; i <= 34; i++)
	{
		read_holding(i, 0);
		if (fflag)id_list[indx++] = i;
		else cnt++;
		if (cnt >= 3)break;
		delay(1);
	}
	PRINTFN("SLAVE COUNT IS %d", indx);
	return indx;
}

uint8_t send_all(uint8_t addr, uint16_t val)
{
	for (uint8_t i = 0; i < slave_cnt; i++)
	{
		write_holding_reg(id_list[i], addr, val);
		if (slave_cnt > 1)delay(50);
	}
	return 1;
}


uint8_t send_all_windows(uint8_t addr, uint16_t val)
{
	for (uint8_t i = 0; i < slave_cnt; i++)
	{
		write_holding_reg(id_list[i], addr, val);
		if (slave_cnt > 1)delay(50);
	}
	return 1;
}


uint8_t selected_id()
{
	return id_list[modbus_data[mb_sel_id] - 1];
}
uint8_t selected_window() {
	return modbus_data[mb_sel_win] - 1;
}

void debug_messages()
{
	DBG("WIND SPEED: "); Serial.println(modbus_data[0]);
	DBG("WIND SPEED LIMIT: "); Serial.println(modbus_data[1]);
	DBG("SELECTED ID: "); Serial.println(id_list[modbus_data[2] - 1]);
	DBG("SELECTED WIN: "); Serial.println(modbus_data[3]);
	DBG("SELECTED CMD: "); Serial.println(modbus_data[4]);
	DBG("SAVE FLAG: "); Serial.println(modbus_data[5]);
	DBG("EEPROM SAVE FLAG: "); Serial.println(modbus_data[6]);
	DBG("LIM OVER FLAG: "); Serial.println(modbus_data[7]);
	DBG("MANUAL CONTROL: "); Serial.println(modbus_data[8]);
}
bool is_manual()
{
	return modbus_data[mb_manualcont] == 2;
}
bool single_config()
{
	return modbus_data[mb_win_cmd] < 3;

}
bool valid_id_selected()
{
	return (id_list[modbus_data[mb_sel_id] - 1] > 1) && (modbus_data[mb_sel_id] > 0);

}
bool save_pushed()
{
	return modbus_data[mb_win_save_flag] != 0;

}
void setup()
{
	modbus_data[mb_sel_id] = 1;
	modbus_data[mb_sel_win] = 1;
	pinMode(mb_master_de, OUTPUT);

	DEBUG_SERIAL.begin(debug_baud);
	MB_MASTER_SERIAL.begin(mb_master_baud);
	mb_master.begin(1, MB_MASTER_SERIAL);
	mb_master.preTransmission(mb_master_pre_transmission);
	mb_master.postTransmission(mb_master_post_transmission);
	MB_SLAVE_SERIAL.begin(mb_slave_baud);

	slave_cnt = get_slave_count();
	for (uint8_t i = 0; i < slave_cnt; i++)PRINTFN("ID #%d: %d", i, id_list[i]);
	mb_slave.start();
}

void loop()
{
	modbus_data[mb_win_save_flag] = 0;

	mb_slave.poll(modbus_data, mb_data_size);



	if (millis() - pperiod >= 5000)
	{
		pperiod = millis();
		//debug_messages();
	}

	if (modbus_data[mb_windspeed] > modbus_data[mb_windspeedlim])
	{
		modbus_data[mb_lim_over_flag] = 1;
	}
	else { modbus_data[mb_lim_over_flag] = 0; }




	if (single_config())
	{


		if (save_pushed() && valid_id_selected())
		{

			if (is_manual())
			{
				write_holding_reg(selected_id(), mb_lim_over, 1);
			}

			write_holding_reg(selected_id(), selected_window(), modbus_data[mb_win_cmd]);

			if (modbus_data[mb_eeprom_save_flag] != 0)
			{
				write_holding_reg(id_list[modbus_data[mb_sel_id] - 1], mb_eflag, 1);
				modbus_data[mb_eeprom_save_flag] = 0;
			}
			modbus_data[mb_win_save_flag] = 0;
		}

	}
	else
	{
		if (save_pushed())
		{
			if (is_manual())
			{
				if (!man_flag) {
					send_all(mb_lim_over, 1);
					man_flag = true;
					anti_done_once = false;
					done_once = false;
				}
			}
			else
			{
				if (man_flag) {
					send_all(mb_lim_over, 0);
					man_flag = false;
				}
				if (!done_once)
				{
					if (modbus_data[mb_lim_over_flag] == 1) {
						send_all(mb_lim_over, 1);
					}

					anti_done_once = false;
					done_once = true;
				}
				if (!anti_done_once)
				{
					if (modbus_data[mb_lim_over_flag] == 0) {
						send_all(mb_lim_over, 0);
					}
					anti_done_once = true;
					done_once = false;

				}
			}
			switch (modbus_data[mb_win_cmd])
			{
			case 3: send_all(mb_w1_cmd, 0);
				send_all(mb_w2_cmd, 0);
				break;
			case 4: send_all(mb_w1_cmd, 1);
				send_all(mb_w2_cmd, 1);
				break;
			case 5: send_all(mb_w1_cmd, 2);
				send_all(mb_w2_cmd, 2);
				break;
			default: break;
			}
			if (modbus_data[mb_eeprom_save_flag] != 0)
			{
				write_holding_reg(selected_id(), mb_eflag, 1);
				modbus_data[mb_eeprom_save_flag] = 0;
			}
			modbus_data[mb_win_save_flag] = 0;
		}
	}
}


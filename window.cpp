#include "window.h"
#include <Arduino.h>
window::window(uint8_t _directionPin, uint8_t _pwmPinCW, uint8_t _pwmPinCCW)
{
	directionPin = _directionPin;
	pwmPinCW = _pwmPinCW;
	pwmPinCCW = _pwmPinCCW;

	pinMode(directionPin, OUTPUT);
	pinMode(pwmPinCCW, OUTPUT);
	pinMode(pwmPinCW, OUTPUT);

	digitalWrite(directionPin, LOW);
	digitalWrite(pwmPinCW, LOW);
	digitalWrite(pwmPinCCW, LOW);
}
void window::update()
{
	switch (currentState)
	{
	case 0:
		stop();
		break;
	case 1:
		moveCW();
		break;
	case 2:
		moveCCW();
		break;
	}
}
void window::stop()
{
	pwmVal = 0;
	isMovingCCW = false;
	isMovingCW = false;
	stopState = true;
	analogWrite(pwmPinCCW, 0);
	analogWrite(pwmPinCW, 0);
	digitalWrite(directionPin, 0);
	periodAfterStop = millis();

}

void window::moveCCW()
{
	if (isMovingCW)
		stop();
	if (millis() - periodAfterStop >= maxPeriodAfterStop) {
		stopState = false;
	}
	if (!stopState) {
		isMovingCCW = true;
		digitalWrite(directionPin, LOW);
		if (millis() - periodBetweenPwm >= maxPeriodBetweenPWM)
		{
			periodBetweenPwm = millis();
			if (pwmVal == maxPossiblePwm)
				analogWrite(pwmPinCCW, maxPossiblePwm);
			else
			{
				analogWrite(pwmPinCCW, pwmVal++);
			}
		}
	}
}
void window::moveCW()
{
	if (isMovingCCW)
	{
		stop();
	}
	if (millis() - periodAfterStop >= maxPeriodAfterStop) {
		stopState = false;
	}

	if (!stopState) {
		isMovingCW = true;
		digitalWrite(directionPin, HIGH);
		if (millis() - periodBetweenPwm > maxPeriodBetweenPWM)
		{
			periodBetweenPwm = millis();
			if (pwmVal == maxPossiblePwm)
				analogWrite(pwmPinCW, maxPossiblePwm);
			else
			{
				analogWrite(pwmPinCW, pwmVal++);
			}
		}
	}
}
void window::setState(uint16_t state)
{
	currentState = state;
}
void window::setMaxPeriodBetweenPWM(unsigned long t)
{
	maxPeriodBetweenPWM = t;
}
void window::setMaxPossiblePwm(uint8_t t)
{
	maxPossiblePwm = t;
}
void window::setPeriodAfterStop(unsigned long t)
{
	maxPeriodAfterStop = t;
}
#ifndef WINDOW_H
#define WINDOW_H
#include <inttypes.h>
class window
{
private:
	uint8_t 
		directionPin = 0, 
		pwmPinCW = 0, 
		pwmPinCCW = 0, 
		manualControl = 0, 
		currentState = 0, 
		pwmVal = 0, 
		maxPossiblePwm = 250U;


	unsigned long 
		periodBetweenPwm = 0, 
		periodAfterStop=0,
		maxPeriodBetweenPWM = 40UL, 
		maxPeriodAfterStop = 1000UL;

	bool 
		isMovingCCW = false,
		isMovingCW = false, 
		stopState = true;

public:
	window(uint8_t _directionPin, uint8_t _pwmPinCW, uint8_t _pwmPinCCW);

	void moveCW();
	void moveCCW();
	void stop();
	void update();
	void setState(uint16_t state);
	void setMaxPeriodBetweenPWM(unsigned long t);
	void setPeriodAfterStop(unsigned long t);
	void setMaxPossiblePwm(uint8_t t);
};
#endif